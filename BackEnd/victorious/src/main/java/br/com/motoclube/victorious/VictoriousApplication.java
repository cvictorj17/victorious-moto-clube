package br.com.motoclube.victorious;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.PathResourceResolver;


@SpringBootApplication
public class VictoriousApplication implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(VictoriousApplication.class, args);
	}

	@Autowired
	private ResourceProperties resourceProperties = new ResourceProperties();

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		long cachePeriod = resourceProperties.getCache().getPeriod().getSeconds();

		final String[] staticLocations = resourceProperties.getStaticLocations();
		final String[] indexLocations = new String[staticLocations.length];
		for (int i = 0; i < staticLocations.length; i++) {
			indexLocations[i] = staticLocations[i] + "index.html";
		}

		registry.addResourceHandler("/**/*.*")
				.addResourceLocations(staticLocations)
				.setCachePeriod(Math.toIntExact(cachePeriod));

		registry.addResourceHandler("/**")
				.addResourceLocations(indexLocations)
				.setCachePeriod(Math.toIntExact(cachePeriod))
				.resourceChain(true)
				.addResolver(new PathResourceResolver() {
					@Override
					protected Resource getResource(String resourcePath,
												   Resource location) {
						return location.exists() && location.isReadable() ? location : null;
					}
				});

		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");

		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

}
