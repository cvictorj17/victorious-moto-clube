package br.com.motoclube.victorious.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket victoriousApi()
    {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("br.com.motoclube.victorious"))
                .paths(regex("/api.*"))
                .build()
                .apiInfo(metaInfo());
    }

    private ApiInfo metaInfo(){
            ApiInfo apiInfo = new ApiInfo(
                    "Vistorios API",
                    "Api Cadastro de Membros",
                    "1.0",
                    "Termos de Serviço",
                    new Contact("Cícero Victor", "","cvictorj17@gmail.com" ),
                    "Apache License Version 2.0",
                    "https://www.apache.org.html",
                    new ArrayList<VendorExtension>()
            );

            return  apiInfo;
    }
}
