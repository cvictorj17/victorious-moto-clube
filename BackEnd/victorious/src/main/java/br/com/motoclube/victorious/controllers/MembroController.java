package br.com.motoclube.victorious.controllers;

import br.com.motoclube.victorious.domain.Membro;
import br.com.motoclube.victorious.domain.Mensalidade;
import br.com.motoclube.victorious.services.MembroService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(value = "/api/membro")
@Api(value="API Rest Membros")
@CrossOrigin(origins = "*")
public class MembroController {

    @Autowired
    MembroService membroService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Incluir Membros")
    public ResponseEntity<Membro>  create(@RequestBody @Valid Membro membro, BindingResult result){
        if (!result.hasErrors()) {
            membro = membroService.create(membro);

        }
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(membro.id).toUri();

        return ResponseEntity.created(uri).body(membro);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Retorna buscando pelo nome")
    public ResponseEntity<Page<Membro>> search(
            @RequestParam(required = false) String nome,
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC, page = 0, size = 20) Pageable pageable) {
        Page<Membro> pages = membroService.search(
                nome,
                pageable);
        return ResponseEntity.ok().body(pages);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Retorna buscando pelo Id")
    public ResponseEntity<Membro> find(@PathVariable Integer id) {
        return ResponseEntity.ok().body(membroService.find(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "Altera um Membro específico")
    public ResponseEntity<Membro> update(@PathVariable Integer id, @RequestBody Membro membro) {
        return ResponseEntity.ok().body(membroService.update(id, membro));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Exclui um Membro específico")
    public ResponseEntity delete(@PathVariable Integer id) {
        membroService.delete(id);
        return ResponseEntity.ok().build();
    }

}
