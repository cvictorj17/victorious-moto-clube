package br.com.motoclube.victorious.controllers;

import br.com.motoclube.victorious.domain.Mensalidade;
import br.com.motoclube.victorious.services.MensalidadeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping(value = "/api/mensalidade")
@Api(value="API Rest Mensalidades")
@CrossOrigin(origins = "*")
public class MensalidadeController {
    @Autowired
    MensalidadeService mensalidadeService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Salva mensalidade")
    public ResponseEntity<Mensalidade> create(@RequestBody Mensalidade mensalidade){
        mensalidade = mensalidadeService.create(mensalidade);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(mensalidade.id).toUri();

        return  ResponseEntity.created(uri).body(mensalidade);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Retorna buscando pelo mês e ano")
    public ResponseEntity<Page<Mensalidade>> search(
            @RequestParam(required = false) String mesAno,
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC, page = 0, size = 20) Pageable pageable) {
        Page<Mensalidade> pages = mensalidadeService.search(
                mesAno,
                pageable);
        return ResponseEntity.ok().body(pages);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Retorna buscando pelo Id")
    public ResponseEntity<Mensalidade> find(@PathVariable Integer id) {
        return ResponseEntity.ok().body(mensalidadeService.find(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "Altera uma mensalidade específica")
    public ResponseEntity<Mensalidade> update(@PathVariable Integer id, @RequestBody Mensalidade mensalidade) {
        return ResponseEntity.ok().body(mensalidadeService.update(id, mensalidade));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Exclui mensalidade pelo Id")
    public ResponseEntity delete(@PathVariable Integer id) {
        mensalidadeService.delete(id);
        return ResponseEntity.ok().build();
    }


}
