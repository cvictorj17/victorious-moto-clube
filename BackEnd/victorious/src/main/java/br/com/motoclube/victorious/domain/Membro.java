package br.com.motoclube.victorious.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Document(collection = "membro")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Membro {

    @Id
    public Integer id;

    @NotNull
    public  String nome;

    public String cargo;

    public Date dataInclusao;

    public String tipoMembro;  //hang, pp, meio colete, coletado

    public String RG;

    public String CPF;

    public String tituloEleitor;

    public String habilitacao;

    public Date dataNascimento;

    public String estadoCivil;  // solteiro, casado, divorciado

    public String numeroCelular;

    public String numeroCelularContato;

    public String nomeContato;

    public String email;

    public String senha;

    public String endereco;

    public String bairro;

    public String cidade;

    public String UF;

    public String CEP;

    @DBRef
    public List<Mensalidade> mensalidades;



}
