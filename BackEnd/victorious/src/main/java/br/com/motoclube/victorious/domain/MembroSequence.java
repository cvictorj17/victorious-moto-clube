package br.com.motoclube.victorious.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "membroSequence")
public class MembroSequence {
    @Id
    public String id;
    public Integer seq;
}
