package br.com.motoclube.victorious.domain;

import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

public class Mensalidade {

    @Id
    public Integer id;

    public String mesAno;

    public Double valor;

    public Date dataPagamento;

    public Date dataInclusao;

    public Integer idMenbro;
    
    public List<Membro> membroList;

}
