package br.com.motoclube.victorious.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "mensalidadeSequence")
public class MensalidadeSequence {
    @Id
    public String id;
    public Integer seq;

}
