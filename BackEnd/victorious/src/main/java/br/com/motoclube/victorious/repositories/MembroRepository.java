package br.com.motoclube.victorious.repositories;

import br.com.motoclube.victorious.domain.Membro;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MembroRepository extends CrudRepository<Membro, Integer> {

}
