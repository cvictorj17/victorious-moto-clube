package br.com.motoclube.victorious.repositories;

import br.com.motoclube.victorious.domain.Mensalidade;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MensalidadeRepository extends CrudRepository<Mensalidade, Integer> {

}
