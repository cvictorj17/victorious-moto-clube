package br.com.motoclube.victorious.services;

import br.com.motoclube.victorious.domain.Membro;
import br.com.motoclube.victorious.domain.MembroSequence;
import br.com.motoclube.victorious.repositories.MembroRepository;
import com.sun.istack.internal.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class MembroService {

    @Autowired
    private MembroRepository membroRepository;

    @Autowired
    private MongoOperations mongo;

    public Membro find(Integer id) {
        return  membroRepository.findById(id).orElse(null);

    }

    public Membro create(@NotNull Membro membro){
        membro.dataInclusao = new Date();

        // Getting next entry id
        MembroSequence counter = mongo.findAndModify(
                query(where("_id").is("membroSequence")),
                new Update().inc("seq", 1),
                options().returnNew(true).upsert(true),
                MembroSequence.class
        );

        assert counter != null;

        membro.id = counter.seq;

        return membroRepository.save(membro);

    }

    public Membro update(Integer idMembro, @NotNull Membro membro)
    {
        membroRepository.findById(idMembro).ifPresent( membro1-> {
            membro.id = idMembro;
            membroRepository.save(membro);
        });

        return membro;
    }

    public void delete(Integer id)
    {
        membroRepository.deleteById(id);

    }

    public Page<Membro> search(
            String nome,
            Pageable pageable
    ) {
        Query query = new Query();

        if (nome != null && !nome.isEmpty()) {
            query.addCriteria(Criteria.where("nome").regex(nome, "i"));
        }

        long count = mongo.count(query, Membro.class);

        List<Membro> membroList = mongo.find(query.with(pageable), Membro.class);

        return PageableExecutionUtils.getPage(membroList, pageable, () -> count);
    }
}
