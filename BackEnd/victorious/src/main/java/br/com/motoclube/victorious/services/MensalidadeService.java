package br.com.motoclube.victorious.services;

import br.com.motoclube.victorious.domain.Mensalidade;
import br.com.motoclube.victorious.domain.MensalidadeSequence;
import br.com.motoclube.victorious.repositories.MensalidadeRepository;
import com.sun.istack.internal.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class MensalidadeService {

    @Autowired
    private MensalidadeRepository mensalidadeRepository;

    @Autowired
    private MongoOperations mongo;

    public Mensalidade find(Integer id) {
        return  mensalidadeRepository.findById(id).orElse(null);
    }

    public Mensalidade create(@NotNull Mensalidade mensalidade){
        mensalidade.dataInclusao = new Date();

        // Getting next entry id
        MensalidadeSequence counter = mongo.findAndModify(
                query(where("_id").is("mensalidadeSequence")),
                new Update().inc("seq", 1),
                options().returnNew(true).upsert(true),
                MensalidadeSequence.class
        );

        assert counter != null;

        mensalidade.id = counter.seq;

        return mensalidadeRepository.save(mensalidade);

    }

    public Mensalidade update(Integer id, @NotNull Mensalidade mensalidade)
    {
        mensalidadeRepository.findById(id).ifPresent( mensalidade1-> {
            mensalidade.id = id;
            mensalidadeRepository.save(mensalidade);
        });

        return mensalidade;
    }

    public void delete(Integer id)
    {
        mensalidadeRepository.deleteById(id);

    }

    public Page<Mensalidade> search(
            String mesAno,
            Pageable pageable
    ) {
        Query query = new Query();

        if (mesAno != null && !mesAno.isEmpty()) {
            query.addCriteria(Criteria.where("mesAno").regex(mesAno));
        }

        long count = mongo.count(query, Mensalidade.class);

        List<Mensalidade> mensalidadeList = mongo.find(query.with(pageable), Mensalidade.class);

        return PageableExecutionUtils.getPage(mensalidadeList, pageable, () -> count);
    }

}
